/*
 * @ file main.c - EFM32ZG IAR Template
 *
 *  Created on: 22/10/2018
 *      Author: AntoMota
 */
 
/* Headers */
#include "em_device.h"
#include "em_system.h"
#include "em_chip.h"
#include "usartcero.h"

/* Functions */

/***************************************************************************//**
 * @brief
 *   Just the main body of the program.
 *
 * @details
 *   In this example the UART is used to send a custom string. This is the
 *   minimum you need to implement a new project.
 *
 * @note
 *   none.
 *
 * @param[in] none
 *   none
 *
 * @return
 *   nothing
 ******************************************************************************/



void main()
{
  
  uint8_t Hola_Tognazo[] = "Hola, Tognazqwertyuiop�lkjhgfdsazxcbnm,o\n";

  cmuSetup0();
  uartSetup0();
  
  
  while(1)
  {
      uartPutData0(Hola_Tognazo, strlen((char const*)Hola_Tognazo));
  }
}


/***************************************************************************//**
 * @brief
 *   Brief description of the function.
 *
 * @details
 *   Detailed descritpion of the function.
 *
 * @note
 *   Comments for things that may not be intuitive.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   What the function yields.
 ******************************************************************************/ 
//int16_t bar(*param 1, *param 2)
//{
//    
//}
